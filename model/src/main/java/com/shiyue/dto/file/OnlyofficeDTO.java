package com.shiyue.dto.file;

import lombok.Data;

@Data
public class OnlyofficeDTO {
    private String fileId;

    private String fileName;

    private String fileUrl;

    private String extendName;
}
