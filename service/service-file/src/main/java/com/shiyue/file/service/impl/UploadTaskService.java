package com.shiyue.file.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.shiyue.domain.UploadTask;
import com.shiyue.file.mapper.UploadTaskMapper;
import com.shiyue.file.service.IUploadTaskService;
import org.springframework.stereotype.Service;

@Service
public class UploadTaskService extends ServiceImpl<UploadTaskMapper, UploadTask> implements IUploadTaskService {


}
