package com.shiyue.file.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.shiyue.domain.CommonFile;
import com.shiyue.vo.commonfile.CommonFileListVo;
import com.shiyue.vo.commonfile.CommonFileUser;


import java.util.List;

public interface ICommonFileService extends IService<CommonFile> {
    List<CommonFileUser> selectCommonFileUser(Long userId);
    List<CommonFileListVo> selectCommonFileByUser(Long userId, Long sessionUserId);
}