package com.shiyue.file.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shiyue.domain.PictureFile;


public interface PictureFileMapper extends BaseMapper<PictureFile> {
}
