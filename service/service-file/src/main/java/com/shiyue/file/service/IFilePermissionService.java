package com.shiyue.file.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.shiyue.domain.FilePermission;


public interface IFilePermissionService extends IService<FilePermission> {

}