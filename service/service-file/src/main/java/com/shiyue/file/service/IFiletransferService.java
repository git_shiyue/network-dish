package com.shiyue.file.service;


import com.shiyue.domain.FileBean;
import com.shiyue.dto.file.DownloadFileDTO;
import com.shiyue.dto.file.PreviewDTO;
import com.shiyue.dto.file.UploadFileDTO;
import com.shiyue.vo.file.UploadFileVo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface IFiletransferService {

    UploadFileVo uploadFileSpeed(UploadFileDTO uploadFileDTO);

    void uploadFile(HttpServletRequest request, UploadFileDTO UploadFileDto, Long userId);

    void downloadFile(HttpServletResponse httpServletResponse, DownloadFileDTO downloadFileDTO);

    void downloadUserFileList(HttpServletResponse httpServletResponse, String filePath, String fileName, List<String> userFileIds);

    void previewFile(HttpServletResponse httpServletResponse, PreviewDTO previewDTO);

    void previewPictureFile(HttpServletResponse httpServletResponse, PreviewDTO previewDTO);

    void deleteFile(FileBean fileBean);

    Long selectStorageSizeByUserId(Long userId);
}
