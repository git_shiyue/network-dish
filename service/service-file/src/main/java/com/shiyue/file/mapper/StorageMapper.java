package com.shiyue.file.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shiyue.domain.StorageBean;


public interface StorageMapper  extends BaseMapper<StorageBean> {


}
