package com.shiyue.file.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shiyue.domain.Share;
import com.shiyue.vo.share.ShareListVO;


import java.util.List;

public interface ShareMapper  extends BaseMapper<Share> {

    List<ShareListVO> selectShareList(String shareFilePath, String shareBatchNum, Long beginCount, Long pageCount, Long userId);
    int selectShareListTotalCount(String shareFilePath,String shareBatchNum, Long userId);
}
