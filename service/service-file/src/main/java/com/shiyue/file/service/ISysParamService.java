package com.shiyue.file.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.shiyue.domain.SysParam;


public interface ISysParamService  extends IService<SysParam> {
    String getValue(String key);
}
