package com.shiyue.file.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.shiyue.domain.FileBean;
import com.shiyue.vo.file.FileDetailVO;


public interface IFileService  extends IService<FileBean> {

    Long getFilePointCount(String fileId);
    void unzipFile(String userFileId, int unzipMode, String filePath);

    void updateFileDetail(String userFileId, String identifier, long fileSize, long modifyUserId);

    FileDetailVO getFileDetail(String userFileId);

}
