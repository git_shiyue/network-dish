package com.shiyue.file.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.shiyue.domain.UploadTask;

public interface IUploadTaskService extends IService<UploadTask> {

}
