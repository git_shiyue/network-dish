package com.shiyue.file.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.shiyue.domain.UploadTaskDetail;

import java.util.List;

public interface IUploadTaskDetailService  extends IService<UploadTaskDetail> {
    List<Integer> getUploadedChunkNumList(String identifier);
}
