package com.shiyue.file.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shiyue.domain.FilePermission;


public interface FilePermissionMapper extends BaseMapper<FilePermission> {

}
