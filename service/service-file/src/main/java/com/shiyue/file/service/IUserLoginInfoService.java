package com.shiyue.file.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.shiyue.domain.UserLoginInfo;

public interface IUserLoginInfoService extends IService<UserLoginInfo> {

}
