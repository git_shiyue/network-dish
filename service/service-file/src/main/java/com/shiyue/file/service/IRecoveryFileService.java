package com.shiyue.file.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.shiyue.domain.RecoveryFile;
import com.shiyue.vo.file.RecoveryFileListVo;


import java.util.List;

public interface IRecoveryFileService extends IService<RecoveryFile> {
    void deleteUserFileByDeleteBatchNum(String deleteBatchNum);
    void restorefile(String deleteBatchNum, String filePath, Long sessionUserId);
    List<RecoveryFileListVo> selectRecoveryFileList(Long userId);
}
