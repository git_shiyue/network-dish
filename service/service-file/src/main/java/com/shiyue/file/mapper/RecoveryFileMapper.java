package com.shiyue.file.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.shiyue.domain.RecoveryFile;
import com.shiyue.vo.file.RecoveryFileListVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface RecoveryFileMapper extends BaseMapper<RecoveryFile> {
    List<RecoveryFileListVo> selectRecoveryFileList(@Param("userId") Long userId);
}
