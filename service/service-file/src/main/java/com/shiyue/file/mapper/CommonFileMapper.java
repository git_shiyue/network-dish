package com.shiyue.file.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.shiyue.domain.CommonFile;
import com.shiyue.vo.commonfile.CommonFileListVo;
import com.shiyue.vo.commonfile.CommonFileUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CommonFileMapper extends BaseMapper<CommonFile> {
    List<CommonFileUser> selectCommonFileUser(@Param("userId") Long userId);
    List<CommonFileListVo> selectCommonFileByUser(@Param("userId") Long userId, @Param("sessionUserId") Long sessionUserId);

}
