package com.shiyue.file.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.shiyue.domain.Share;
import com.shiyue.dto.sharefile.ShareListDTO;
import com.shiyue.vo.share.ShareListVO;


import java.util.List;

public interface IShareService  extends IService<Share> {
    List<ShareListVO> selectShareList(ShareListDTO shareListDTO, Long userId);
    int selectShareListTotalCount(ShareListDTO shareListDTO, Long userId);
}
