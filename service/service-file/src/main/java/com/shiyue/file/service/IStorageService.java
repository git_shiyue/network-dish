package com.shiyue.file.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.shiyue.domain.StorageBean;


public interface IStorageService extends IService<StorageBean> {
    Long getTotalStorageSize(Long userId);
    boolean checkStorage(Long userId, Long fileSize);
}
