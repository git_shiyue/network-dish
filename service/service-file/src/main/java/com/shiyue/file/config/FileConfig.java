package com.shiyue.file.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: 张祯
 * @Description: 配置类
 * @CreateTime: 2022-11-25  21:32
 * @Version: 1.0
 */

@MapperScan("com.shiyue.file.mapper")
@ComponentScan("com.shiyue")
@Configuration
public class FileConfig {
}
