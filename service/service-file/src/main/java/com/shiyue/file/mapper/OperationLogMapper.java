package com.shiyue.file.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shiyue.domain.OperationLogBean;


public interface OperationLogMapper extends BaseMapper<OperationLogBean> {

}
