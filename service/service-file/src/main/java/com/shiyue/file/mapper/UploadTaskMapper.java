package com.shiyue.file.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shiyue.domain.UploadTask;


public interface UploadTaskMapper extends BaseMapper<UploadTask> {

}
