package com.shiyue.exception;

import com.shiyue.result.ResultCodeEnum;
import lombok.Data;

/**
 * 自定义全局异常类
 */
@Data
public class ShiYueException extends RuntimeException {
    private Integer code;

    public ShiYueException(String message) {
        super(message);
        this.code = ResultCodeEnum.UNKNOWN_ERROR.getCode();
    }

    public ShiYueException(Integer code, String message) {
        super(message);
        this.code = code;
    }

    public ShiYueException(ResultCodeEnum resultCodeEnum) {
        super(resultCodeEnum.getMessage());
        this.code = resultCodeEnum.getCode();
    }

    @Override
    public String toString() {
        return "QiwenException{" + "code=" + code + ", message=" + this.getMessage() + '}';
    }
}