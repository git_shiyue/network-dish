package com.shiyue.ufop.operation.read;


import com.shiyue.ufop.operation.read.domain.ReadFile;

public abstract class Reader {
    public abstract String read(ReadFile readFile);
}
