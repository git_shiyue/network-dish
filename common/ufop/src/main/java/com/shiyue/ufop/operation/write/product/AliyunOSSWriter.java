package com.shiyue.ufop.operation.write.product;

import com.aliyun.oss.OSS;
import com.shiyue.ufop.config.AliyunConfig;
import com.shiyue.ufop.operation.write.Writer;
import com.shiyue.ufop.operation.write.domain.WriteFile;
import com.shiyue.ufop.util.AliyunUtils;
import com.shiyue.ufop.util.UFOPUtils;

import java.io.InputStream;

public class AliyunOSSWriter extends Writer {

    private AliyunConfig aliyunConfig;

    public AliyunOSSWriter(){

    }

    public AliyunOSSWriter(AliyunConfig aliyunConfig) {
        this.aliyunConfig = aliyunConfig;
    }

    @Override
    public void write(InputStream inputStream, WriteFile writeFile) {
        OSS ossClient = AliyunUtils.getOSSClient(aliyunConfig);

        ossClient.putObject(aliyunConfig.getOss().getBucketName(), UFOPUtils.getAliyunObjectNameByFileUrl(writeFile.getFileUrl()), inputStream);
        ossClient.shutdown();
    }



}
