package com.shiyue.ufop.operation.download.domain;

import lombok.Data;


@Data
public class Range {
    private int start;
    private int length;
}
