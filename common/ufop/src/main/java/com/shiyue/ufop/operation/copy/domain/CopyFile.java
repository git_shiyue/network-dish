package com.shiyue.ufop.operation.copy.domain;

import lombok.Data;

@Data
public class CopyFile {
    private String extendName;
}
