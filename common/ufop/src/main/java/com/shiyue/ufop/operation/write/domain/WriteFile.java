package com.shiyue.ufop.operation.write.domain;

import lombok.Data;

@Data
public class WriteFile {
    private String fileUrl;
    private long fileSize;
}
