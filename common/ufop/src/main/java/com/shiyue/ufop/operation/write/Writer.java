package com.shiyue.ufop.operation.write;



import com.shiyue.ufop.operation.write.domain.WriteFile;

import java.io.InputStream;

public abstract class Writer {
    public abstract void write(InputStream inputStream, WriteFile writeFile);
}
