package com.shiyue.ufop.operation.read.product;

import com.shiyue.ufop.exception.operation.ReadException;
import com.shiyue.ufop.operation.read.Reader;
import com.shiyue.ufop.operation.read.domain.ReadFile;
import com.shiyue.ufop.util.ReadFileUtils;
import com.shiyue.ufop.util.UFOPUtils;
import org.apache.commons.io.FilenameUtils;

import java.io.FileInputStream;
import java.io.IOException;

public class LocalStorageReader extends Reader {
    @Override
    public String read(ReadFile readFile) {

        String fileContent;
        try {
            String extendName = FilenameUtils.getExtension(readFile.getFileUrl());
            FileInputStream fileInputStream = new FileInputStream(UFOPUtils.getStaticPath() + readFile.getFileUrl());
            fileContent = ReadFileUtils.getContentByInputStream(extendName, fileInputStream);
        } catch (IOException e) {
            throw new ReadException("文件读取出现异常", e);
        }
        return fileContent;
    }
}
