package com.shiyue.ufop.operation.read.product;

import com.qiniu.util.Auth;
import com.shiyue.ufop.config.QiniuyunConfig;
import com.shiyue.ufop.exception.operation.ReadException;
import com.shiyue.ufop.operation.read.Reader;
import com.shiyue.ufop.operation.read.domain.ReadFile;
import com.shiyue.ufop.util.ReadFileUtils;
import com.shiyue.utils.HttpsUtils;
import org.apache.commons.io.FilenameUtils;

import java.io.IOException;
import java.io.InputStream;

public class QiniuyunKodoReader extends Reader {

    private QiniuyunConfig qiniuyunConfig;

    public QiniuyunKodoReader(){

    }

    public QiniuyunKodoReader(QiniuyunConfig qiniuyunConfig) {
        this.qiniuyunConfig = qiniuyunConfig;
    }

    @Override
    public String read(ReadFile readFile) {
        String fileUrl = readFile.getFileUrl();
        String fileType = FilenameUtils.getExtension(fileUrl);
        try {
            return ReadFileUtils.getContentByInputStream(fileType, getInputStream(readFile.getFileUrl()));
        } catch (IOException e) {
            throw new ReadException("读取文件失败", e);
        }
    }

    public InputStream getInputStream(String fileUrl) {
        Auth auth = Auth.create(qiniuyunConfig.getKodo().getAccessKey(), qiniuyunConfig.getKodo().getSecretKey());

        String urlString = auth.privateDownloadUrl(qiniuyunConfig.getKodo().getDomain() + "/" + fileUrl);



        return HttpsUtils.doGet(urlString, null);
    }


}
