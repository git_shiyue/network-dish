package com.shiyue.gatway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author: 张祯
 * @Description: 网关启动类
 * @CreateTime: 2022-11-25  20:13
 * @Version: 1.0
 */


@SpringBootApplication
public class GatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class);
    }
}
